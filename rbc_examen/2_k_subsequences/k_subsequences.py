# a k subsequence se define asi:
# es un sub arreglo de elementos contiguos
# la suma de los elementos es evenly divisible by k: sum % k = 0
# ejemplo:
# k = 5
# arreglo = [5,10,11,9,5]
# las 10 k subsecuencias son:
# {5}
# {5,10} porque 15 % 5 = 0
# {5,10,11,9} porque 30 % 5 = 0
# {5,10,11,9,5} porque son 35 y 35%5 = 0
# {10}
# {10,11,9} = 30%5=0
# {10,11,9,5} = 35%5=0
# {11,9}
# {11,9,5}
# {5} si dos veces porque hay dos 5 en el arreglo
# Puedes basarte en el ejercicio anterior para obtener todos los
# subarreglos y luego les haces modulo para ver si son o no.
# Otro ejemplo
# k = 3
# [5,1,2,3,4,1]
# Answer is: 4
# Because:
# {3}, {1,2}, {1,2,3}, {2,3,4}

def kSub(k, nums):
    result = 0
    for x in range(0,len(nums)):
        for y in range(0,len(nums)-x):
            suma = 0
            for z in range(0,x+1):
                suma = suma + nums[y+z]
            if suma%k==0:
                result = result + 1
    return result

if __name__ == '__main__':

    # Test Case 1
    k = 3
    nums = [5,1,2,3,4,1]
    result = kSub(k,nums)
    if result == 4:
        print("PASS")
    else:
        print("FAIL: ")
        print(result)

    # Test Case 2
    k = 5
    nums = [5,10,11,9,5]
    result = kSub(k,nums)
    if result == 10:
        print("PASS")
    else:
        print("FAIL: ")
        print(result)