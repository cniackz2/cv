import pdb

def target_in_source(target,temporal_array):
    result = True
    for item in target:
        if item in temporal_array:
            # lo sacas porque quieres saber que todas las letras esten dentro del substring
            # sino lo sacas entonces krr estara dentro de ker pero esta mal porque necesitas dos r no una
            # temporal_array.pop() no hagas pop porque no saca la letra que quieres.
            # busca como remover la letra que realmente quieres.
            temporal_array.remove(item)
        else:
            return False
    return result

def function(target,source):
    result = 0
    for x in range(0,len(source)):
        for y in range(0,len(source)):
            temporal_array = []
            for z in range(0,x+1):
                # este try catch es para cuando hace el wrap around
                try:
                    temporal_array.append(source[y+z])
                except:
                    temporal_array.append(source[z-1])
            # guardas la longitud por anticipado porque con el pop que haras mas adelante se modificara la longitud
            longitud_del_arreglo_temporal = len(temporal_array)
            the_target_inside_source = target_in_source(target,temporal_array)
            if the_target_inside_source:
                # la idea es quedarte con el menor
                if result == 0:
                    result = longitud_del_arreglo_temporal
                if result < longitud_del_arreglo_temporal:
                    # stay with same result since is minor
                    pass
                if result > longitud_del_arreglo_temporal:
                    result = longitud_del_arreglo_temporal
    return result

# Test case 1
target = 'krr'
source = 'hackerrank'
result = function(target, source)
if result == 4:
    print('pass')
    print(result)
else:
    print('fail')
    print(result)

# Test case 2
target = 'kha'
source = 'hackerrank'
result = function(target, source)
if result == 3:
    print('pass')
    print(result)
else:
    print('fail')
    print(result)