# Objective: Find all the subarrays in an array and print the sum of all subarrays
# Example:
"""
[1] = 1
[2] = 3
[3] = 6
[4] = 10
[5] = 15
[1,2] = 18
[2,3] = 23
[3,4] = 30
[4,5] = 39
[1,2,3] = 45
[2,3,4] = 54
[3,4,5] = 66
[1,2,3,4] = 76
[2,3,4,5] = 90
[1,2,3,4,5] = 105
105
"""
#!/bin/python3

import math
import os
import random
import re
import sys



#
# Complete the 'subarraySum' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

def subarraySum(arr):
    longitud = len(arr)
    suma = 0
    for x in range(0,longitud):
        for y in range(0,longitud-x):
            print("[",end="")
            for z in range(0,x+1):
                suma = suma + int(arr[y+z])
                if z+1 == x+1:
                    print(str(arr[y+z]),end="")
                else:
                    print(str(arr[y+z])+",",end="")
            print("] = "+str(suma))
    return suma


if __name__ == '__main__':
    import fileinput
    lines = []
    for line in fileinput.input():
        lines.append(line.rstrip("\n\r"))
    number_of_test_cases = int(lines[0].rstrip("\n\r"))
    del lines[0]

    result = subarraySum(lines)
    print(result)

    #fptr.write(str(result) + '\n')

    #fptr.close()