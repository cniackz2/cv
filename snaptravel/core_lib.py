import pdb
import json
import uuid
import time
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#################################################
#                                               #
# To send keys to an element                    #
#                                               #
#################################################
def send_keys(driver, selector, keys, wait=10, enter=False, delay=1):
    try:
        element = WebDriverWait(driver, wait).until(
            EC.presence_of_element_located(
                (
                    By.XPATH, selector
                )
            )
        ).send_keys(keys)
        if enter:
            element = WebDriverWait(driver, wait).until(
                EC.presence_of_element_located(
                    (
                        By.XPATH, selector
                    )
                )
            ).send_keys(Keys.ENTER)
        time.sleep(delay)
    except Exception as e:
        print(e)
        pdb.set_trace()

#################################################
#                                               #
# To wait for element to disappear              #
#                                               #
#################################################
def wait_until_disappears(driver, selector):
    element = wait(driver, selector, stop=False)
    while element:
        element = wait(driver, selector, stop=False)

#################################################
#                                               #
# To wait for element                           #
#                                               #
#################################################
def wait(driver, selector, stop=True, wait=10):
    element = None
    try:
        element = WebDriverWait(driver, wait).until(
            EC.presence_of_element_located(
                (
                    By.XPATH, selector
                )
            )
        )
    except:
        if stop:
            pdb.set_trace()
    return element

#################################################
#                                               #
# Clear input                                   #
#                                               #
#################################################
def clear_input(driver,selector):
    element = wait(driver, selector)
    for x in range(0,len(element.get_attribute('value'))):
        send_keys(driver, selector, Keys.BACKSPACE, delay=0)

#################################################
#                                               #
# To click on an element                        #
#                                               #
#################################################
def click(driver, selector,wait=10):
    try:
        element = WebDriverWait(driver, wait).until(
            EC.element_to_be_clickable(
                (
                    By.XPATH, selector
                )
            )
        ).click()
    except Exception as e:
        print(e)
        pdb.set_trace()
    time.sleep(1)