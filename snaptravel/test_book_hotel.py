import pdb
import json
import time
import unittest
import core_lib
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TestBookHotel(unittest.TestCase):

    selectors = {
        "search_field": "//div/input",
        "search_button": "//a[contains(text(),'Search')]",
        "view_details_button": "(//div[contains(text(),'View Details')])[1]",
        "select_room_button": "//*[contains(text(),'Select Room')]",
        "reserve_button": "(//*[contains(text(),'Reserve')])[1]",
        "continue_button": '//div[@id="btn-continue"]',
        "first_name": '//input[@id="first-name"]',
        "last_name": '//input[@id="last-name"]',
        "email": '//input[@id="email"]',
        "phone_number": '//input[@id="phone-number"]',
        "next_button": '//*[@id="btn-next-to-payment"]',
        "card_number": '//*[@id="pan"]',
        "expiry_date": '//*[@id="expiry-year"]',
        "cvv": '//*[@id="cvv"]',
        "name_on_card": '//*[@id="billing-name"]',
        "billing_address": '//*[@id="billing-address"]',
        "complete_booking": '//*[@id="submit-payment"]',
        "work_in_progress_message": "//*[contains(text(), 'do not press back')]",
        "booking_failed": "//*[contains(text(), 'Booking Failed')]",
        "sold_out": "//*[contains(text(), 'has sold out.')]"
    }

    def test_basic_booking(self):
        selectors = self.selectors
        with open('data.json', 'r') as fp:
            data = json.load(fp)

        #######################################
        #                                     #
        # 1. Go to Search Page                #
        #                                     #
        #######################################
        driver = webdriver.Chrome()
        driver.get(data['search_page'])

        #######################################
        #                                     #
        # 2. Perform a serach for Las Vegas   #
        #                                     #
        #######################################
        core_lib.clear_input(driver, selectors['search_field'])
        core_lib.send_keys(driver, selectors['search_field'], data['city'], enter=True)
        core_lib.click(driver, selectors['search_button'])

        #######################################
        #                                     #
        # 3. Choose any hotel                 #
        #                                     #
        #######################################
        core_lib.click(driver, selectors['view_details_button'],15)
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[1]) # switch to new tab

        #######################################
        #                                     #
        # 4. Choose any room                  #
        #                                     #
        #######################################
        core_lib.click(driver, selectors['select_room_button'])
        core_lib.click(driver, selectors['reserve_button'])

        #######################################
        #                                     #
        # 5. Make a booking                   #
        #                                     #
        #######################################
        core_lib.clear_input(driver, selectors['first_name'])
        core_lib.send_keys(driver, selectors['first_name'], data['first_name'])
        core_lib.clear_input(driver, selectors['last_name'])
        core_lib.send_keys(driver, selectors['last_name'], data['last_name'])
        core_lib.clear_input(driver, selectors['email'])
        core_lib.send_keys(driver, selectors['email'], data['email'])
        core_lib.clear_input(driver, selectors['phone_number'])
        core_lib.send_keys(driver, selectors['phone_number'], data['phone_number'])
        core_lib.click(driver, selectors['next_button'])

        #######################################
        #                                     #
        # 6. Complete a booking               #
        #                                     #
        #######################################
        element = core_lib.wait(driver,'//*[@id="iframe"]')
        driver.switch_to.frame(element)
        core_lib.send_keys(driver, selectors['card_number'], data['card_number'])
        driver.switch_to.default_content()
        core_lib.send_keys(driver, selectors['expiry_date'], data['expiry_date'])
        core_lib.send_keys(driver, selectors['cvv'], data['cvv'])
        core_lib.send_keys(driver, selectors['name_on_card'], data['name_on_card'])
        core_lib.send_keys(driver, selectors['billing_address'], data['billing_address'])
        core_lib.click(driver, '//ul/li[1]')
        core_lib.click(driver, selectors['complete_booking'])
        core_lib.wait_until_disappears(driver, selectors['work_in_progress_message'])

        #######################################
        #                                     #
        # 7. Verify booking                   #
        #                                     #
        #######################################
        if core_lib.wait(driver, selectors['booking_failed'], stop=False) != None:
            if core_lib.wait(driver, selectors['sold_out'], stop=False) != None:
                pass # expected behavior
            else:
                fail('booking failed but it is not sold out')
        else:
            pass # expected behavior

if __name__ == '__main__':
    unittest.main()