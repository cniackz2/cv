# https://www.hackerrank.com/challenges/python-string-formatting/problem
def print_formatted(number):
    ancho1 = len(bin(number)[2:])
    ancho = "{:>" + str(ancho1) + "}"
    anche = "{:>" + str(ancho1) + "X" + "}"
    format_specifier = f"'{ancho} {ancho} {anche} {ancho}'.format(i, oct(i)[2:], i, bin(i)[2:])"
    for i in range(1,number+1):
        #line_new = '{:>5}  {:>5} {:>5X} {:>5}'.format(i, oct(i)[2:], i, bin(i)[2:])
        #import pdb; pdb.set_trace()
        line_new = eval(format_specifier)
        print(line_new)

print_formatted(2)

