"""
https://www.hackerrank.com/challenges/find-a-string/problem
In this challenge, the user enters a string and a substring. You have to print
the number of times that the substring occurs in the given string. String
traversal will take place from left to right, not from right to left.
NOTE: String letters are case-sensitive.
"""
def count_substring(string, sub_string):
    if sub_string in string:
        counter = 0
        for x in range(0,len(string),1):
            flag = None
            for y in range(0,len(sub_string),1):
                if (x+y) >= (len(string)):
                    #import pdb; pdb.set_trace()
                    flag = False
                    break
                if string[x+y] == sub_string[y]:
                    # si todos caen aqui es mas uno
                    # import pdb; pdb.set_trace()
                    flag = True
                else:
                    # con que uno caiga fuera no se cuenta
                    flag = False
                    break
            if flag:
                counter = counter + 1
        return counter
    else:
        return 0

if __name__ == '__main__':
    string = 'ABCDCDC'.strip()
    sub_string = 'CDC'.strip()
    count = count_substring(string, sub_string)
    print(count)