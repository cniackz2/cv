#https://www.hackerrank.com/challenges/alphabet-rangoli/problem
import math
import string

def print_rangoli(size):
    import string
    alphabet = string.ascii_lowercase

    for j in range((size-1),0,-1):

        new_string = ''
        for x in range(size-1,j,-1):
            new_string = new_string + alphabet[x] + '-'
        for x in range(j,size):
            new_string = new_string + alphabet[x] + '-'
        print(new_string.center(((size-1)*4)+1,'-'))

    for j in range(0,size,1):

        new_string = ''
        for x in range(size-1,j,-1):
            new_string = new_string + alphabet[x] + '-'
        for x in range(j,size):
            if x == size-1:
                new_string = new_string + alphabet[x]
            else:
                new_string = new_string + alphabet[x] + '-'
        print(new_string.center(((size-1)*4)+1,'-'))

print_rangoli(5)

'''
--------e--------
------e-d-e------
----e-d-c-d-e----
--e-d-c-b-c-d-e--
e-d-c-b-a-b-c-d-e
--e-d-c-b-c-d-e--
----e-d-c-d-e----
------e-d-e------
--------e--------

--------e--------
------e-d-e------
----e-d-c-d-e----
--e-d-c-b-c-d-e--
e-d-c-b-a-b-c-d-e-
--e-d-c-b-c-d-e--
----e-d-c-d-e----
------e-d-e------
--------e--------
'''