if __name__ == '__main__':
    s = 'qA2'

    # 1
    flag_entro = False
    for item in s:
        if item.isalnum():
            print("True")
            flag_entro = True
            break
    if flag_entro is False:
        print("False")

    # 2
    flag_entro = False
    for item in s:
        if item.isalpha():
            print("True")
            flag_entro = True
            break
    if flag_entro is False:
        print("False")

    # 3
    flag_entro = False
    for item in s:
        if item.isdigit():
            print("True")
            flag_entro = True
            break
    if flag_entro is False:
        print("False")

    # 4
    flag_entro = False
    for item in s:
        if item.islower():
            print("True")
            flag_entro = True
            break
    if flag_entro is False:
        print("False")

    # 5
    flag_entro = False
    for item in s:
        if item.isupper():
            print("True")
            flag_entro = True
            break
    if flag_entro is False:
        print("False")
