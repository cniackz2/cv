#https://www.hackerrank.com/challenges/text-wrap/problem
string = 'ABCDEFGHIJKLIMNOQRSTUVWXYZ'
max_width = 4
def wrap(string, max_width):
    sub_string = ''
    paragraph = ''
    for y in range(0,len(string),max_width):
        for x in range(y,max_width+y,1):
            try:
                sub_string = sub_string + string[x]
            except:
                pass
        paragraph = paragraph + sub_string + '\n'
        sub_string = ''
    return paragraph
print(wrap(string, max_width))