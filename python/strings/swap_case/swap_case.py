# https://www.hackerrank.com/challenges/swap-case/problem
# Www.HackerRank.com → wWW.hACKERrANK.COM
# Pythonist 2 → pYTHONIST 2
def swap_case(st):
    nw = []
    for item in st:
        if item.islower():
            nw.append(item.upper())
        elif item.isupper():
            nw.append(item.lower())
        else:
            nw.append(item)
    nw = ''.join(nw)
    return nw

if __name__ == '__main__':
    st = 'Www.HackerRank.com'
    result = swap_case(st)
    print(result)