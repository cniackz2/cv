#!/bin/python3
# https://www.hackerrank.com/challenges/capitalize/problem
# chris alan => Chris Alan

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solve(string):
    string = string.split(" ")
    #import pdb; pdb.set_trace()
    tmp = ''
    for name in string:
        if name != "":
            first_pedacito = True
            for pedacito in name:
                #import pdb; pdb.set_trace()
                if pedacito.islower() and first_pedacito:
                    tmp = tmp + pedacito.upper()
                else:
                    tmp = tmp + pedacito
                first_pedacito = False
            tmp = tmp + " "
        else:
            tmp = tmp + " "
    return tmp

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = 'hello world'

    result = solve(s)

    print(result)

    #fptr.write(result + '\n')

    #fptr.close()
