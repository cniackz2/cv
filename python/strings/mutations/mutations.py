#https://www.hackerrank.com/challenges/python-mutations/problem
string = 'abracadabra'
position = 5
character = 'k'

def mutate_string(string, position, character):
    lista = list(string)
    lista[position] = character
    string = ''.join(lista)
    return string

print(mutate_string(string, position, character))