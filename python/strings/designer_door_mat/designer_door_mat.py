'''
012345678901234
------------.|.------------
---------.|..|..|.---------
------.|..|..|..|..|.------
---.|..|..|..|..|..|..|.---
----------WELCOME----------
---.|..|..|..|..|..|..|.---
------.|..|..|..|..|.------
---------.|..|..|.---------
------------.|.------------
'''
import fileinput
size = None
for line in fileinput.input():
    size = line
#import pdb; pdb.set_trace()
N = int(size.split(' ')[0]) # El vertical
M = int(size.split(' ')[1]) # El horizontal
for y in range(0,N,1):
    for x in range(0,M,1):
        if (x >= ((M//2)-(y+1)-(y*2))) and (x <= ((M//2)+(y+1)+(y*2))) and (y<(N//2)):
            # este es el triangulo superior
            if x%3==1:
                if M%2==0:
                    print('.',end='')
                else:
                    print('|',end='')
            else:
                if M%2==0:
                    print('.',end='')
                else:
                    print('.',end='')
        elif (y>(N//2)):
            # triangulo inferior invertido
            if (x < ((y-(N//2))*3)):
                # parte izquierda del triangulo
                print('-',end='')
            else:
                pass
                if x > (M-1-((y-(N//2))*3)):
                    # parte derecha del triangulo
                    print('-',end='')
                    pass
                else:
                    if x%3==1:
                        if M%2==0:
                            print('.',end='')
                        else:
                            print('|',end='')
                    else:
                        if M%2==0:
                            print('|',end='')
                        else:
                            print('.',end='')
        elif (y==(N//2)):
            # parte central donde va el welcome
            if (M//2)-3 == x:
                print('W',end='')
            elif (M//2)-2 == x:
                print('E',end='')
            elif (M//2)-1 == x:
                print('L',end='')
            elif (M//2)+0 == x:
                print('C',end='')
            elif (M//2)+1 == x:
                print('O',end='')
            elif (M//2)+2 == x:
                print('M',end='')
            elif (M//2)+3 == x:
                print('E',end='')
            else:
                print('-',end='')
        else:
            # relleno
            print('-',end='')
    print('')