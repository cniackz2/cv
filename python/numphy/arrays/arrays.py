#https://www.hackerrank.com/challenges/np-arrays/problem
import pdb
import numpy

def arrays(arr):
    half = len(arr)//2
    for x in range(0,half):
        temp = None
        temp = arr[x]
        arr[x] = arr[len(arr)-1-x]
        arr[len(arr)-1-x] = temp
    a = numpy.array(arr,float)
    return a

entrada = '1 2 3 4 -8 -10'
arr = entrada.split(' ')
result = arrays(arr)
print(result)