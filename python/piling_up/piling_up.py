# test case 3 => falla
# test case 4 => falla
# test case 0 => pasa
# test case 1 => pasa
# test case 2 => pasa
# https://www.hackerrank.com/challenges/piling-up/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
"""
There is a horizontal row of  cubes. The length of each cube is given. You need
to create a new vertical pile of cubes. The new pile should follow these
directions: if  is on top of  then .
When stacking the cubes, you can only pick up either the leftmost or the
rightmost cube each time. Print "Yes" if it is possible to stack the cubes.
Otherwise, print "No". Do not print the quotation marks.
Input Format
The first line contains a single integer , the number of test cases.
For each test case, there are  lines.
The first line of each test case contains , the number of cubes.
The second line contains  space separated integers, denoting the sideLengths of
each cube in that order.
Constraints
Output Format
For each test case, output a single line containing either "Yes" or "No" without
the quotes.
Sample Input
2
6
4 3 2 1 3 4
3
1 3 2
Sample Output
Yes
No
Explanation
In the first test case, pick in this order: left - , right - , left - , right -
, left - , right - .
In the second test case, no order gives an appropriate arrangement of vertical
cubes.  will always come after either  or .
"""
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip("\n\r"))
number_of_test_cases = int(lines[0].rstrip("\n\r"))
del lines[0]
test_cases = lines
def get_the_biggest(a,b):
    if a > b:
        return a
    if b > a:
        return b
    if a == b:
        return a

def compare_from_outside_to_inside(lista):

    flag = "No"
    lista = lista.split(' ')
    lista = list(map(int, lista))
    last_index = len(lista)-1
    left_value = None
    right_value = None

    # Regla 0: Que los externos tengan el numero mayor
    # Caso 1: [ x y z] en donde los extremos son: (x,z) y el interno es: (y)
    # Que el mayor de los extremos sea mayor a (y)
    # Caso 2: [ a b c d ] extremos: (a,b) internos: (b,c)
    # Que el mayor de los extremos sea mayor a los internos

    for x in range(0,last_index):
        biggest_external_number = get_the_biggest(lista[x],lista[last_index-x])
        biggest_internal_number = get_the_biggest(lista[x+1],lista[last_index-x-1])
        if x != last_index-x and x < last_index-x:
            if biggest_external_number >= biggest_internal_number:
                flag = "Yes"
            else:
                break
    print(flag)

for x in range(0,len(test_cases)):
    if x%2 != 0:
        compare_from_outside_to_inside(test_cases[x])
