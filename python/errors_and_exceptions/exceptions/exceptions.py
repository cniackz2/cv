#https://www.hackerrank.com/challenges/exceptions/problem
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
for x in range(1,len(lines)):
    try:
        a = int(lines[x].split(' ')[0])
        b = int(lines[x].split(' ')[1])
        try:
            c = a//b
            print(c)
        except:
            print("Error Code: integer division or modulo by zero")
    except Exception as e:
        print("Error Code:",e)
