# https://www.hackerrank.com/challenges/most-commons/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
# Practice > Python > Collections > Company Logo
"""
A newly opened multinational brand has decided to base their company logo on the three most common characters in the company name. They are now trying out various combinations of company names and logos based on this condition. Given a string , which is the company name in lowercase letters, your task is to find the top three most common characters in the string.

Print the three most common characters along with their occurrence count.
Sort in descending order of occurrence count.
If the occurrence count is the same, sort the characters in alphabetical order.
For example, according to the conditions described above,

 would have it's logo with the letters .

Input Format

A single line of input containing the string .

Constraints

Output Format

Print the three most common characters along with their occurrence count each on a separate line. 
Sort output in descending order of occurrence count. 
If the occurrence count is the same, sort the characters in alphabetical order.

Sample Input 0

aabbbccde
Sample Output 0

b 3
a 2
c 2
Explanation 0


Here, b occurs  times. It is printed first.
Both a and c occur  times. So, a is printed in the second line and c in the third line because a comes before c in the alphabet.

Note: The string  has at least  distinct characters.
"""
#!/bin/python3
import pdb
import collections
import math
import os
import random
import re
import sys

def function(s):
    dic = {}

    # To count
    for item in s:
        try:
            dic[item] = dic[item] + 1
        except:
            dic[item] = 1

    # To order by value
    queue = collections.OrderedDict(sorted(dic.items(), key=lambda t: t[1]))
    items = list(queue.items())

    # Agrupalas por numero
    values = set(map(lambda x:x[1],items))

    # Order alphabetic
    tmp = []
    values = list(values)
    values.sort()
    counter = 0
    flag_stop = False
    for x in reversed(values):
        for item in items:
            if item[1] == x:
                tmp.append(item)
        tmp.sort()
        for item in tmp:
            counter = counter + 1
            print(item[0],item[1])
            if counter == 3:
                flag_stop = True
                break
        if flag_stop:
            break
        else:
            tmp = []

if __name__ == '__main__':
    s = 'qwertyuiopasdfghjklzxcvbnm'
    function(s)