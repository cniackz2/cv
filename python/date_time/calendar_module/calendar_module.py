#https://www.hackerrank.com/challenges/calendar-module/problem
import datetime
import calendar
import fileinput
for line in fileinput.input():
    #print(line)
    pass
#import pdb; pdb.set_trace()
line = line.split(' ')
month = int(line[0])
day = int(line[1])
year = int(line[2])
#import pdb; pdb.set_trace()
x = datetime.datetime(year, month, day,1,1,1)
day_letters = x.weekday()
if day_letters == 6:
    print("Sunday".upper())
if day_letters == 0:
    print("Monday".upper())
if day_letters == 1:
    print("Tuesday".upper())
if day_letters == 2:
    print("Wednesday".upper())
if day_letters == 3:
    print("Thursday".upper())
if day_letters == 4:
    print("Friday".upper())
if day_letters == 5:
    print("Saturday".upper())