#https://www.hackerrank.com/challenges/class-2-find-the-torsional-angle/problem
import math

class Points(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __sub__(self, no):
        x = self.x - no.x
        y = self.y - no.y
        z = self.z - no.z
        return Points(x,y,z)

    def dot(self, no):
        x = self.x * no.x
        y = self.y * no.y
        z = self.z * no.z
        return (x+y+z)

    def cross(self, no):
        x = ((self.y * no.z) - (no.y * self.z))
        y = -((self.x * no.z) - (no.x * self.z))
        z = ((self.x * no.y) - (no.x * self.y))
        return Points(x,y,z)

    def absolute(self):
        return pow((self.x ** 2 + self.y ** 2 + self.z ** 2), 0.5)

if __name__ == '__main__':
    points = [[0.0, 4.0, 5.0], [1.0, 7.0, 6.0], [0.0, 5.0, 9.0], [1.0, 7.0, 2.0]]
    a, b, c, d = Points(*points[0]), Points(*points[1]), Points(*points[2]), Points(*points[3])
    x = (b - a).cross(c - b)
    y = (c - b).cross(d - c)
    yaya = x.dot(y)
    yeye = (x.absolute() * y.absolute())
    import pdb; pdb.set_trace()
    yoyo = yaya / yeye
    angle = math.acos(yoyo)

    print("%.2f" % math.degrees(angle))