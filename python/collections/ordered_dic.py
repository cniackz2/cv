#https://www.hackerrank.com/challenges/py-collections-ordereddict/problem
import pdb
import fileinput
from collections import OrderedDict
ordered_dictionary = OrderedDict()
counter = 0
for line in fileinput.input():
    line = line.rstrip()
    if counter != 0:
        #pdb.set_trace()
        line = line.split()
        key = line[:-1]
        key = " ".join(key)
        value = int(line[-1])
        try:
            ordered_dictionary[key] = ordered_dictionary[key] + value
        except:
            ordered_dictionary[key] = value
    counter = counter + 1
for key in ordered_dictionary:
    print(key,ordered_dictionary[key])