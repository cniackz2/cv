#https://www.hackerrank.com/challenges/defaultdict-tutorial/problem
import pdb
import fileinput
lista = []
for line in fileinput.input():
    lista.append(line.rstrip())
m = int(lista[0].split(' ')[0])
n = int(lista[0].split(' ')[1])
m_words = []
n_words = []
for x in range(1,m+1):
    m_words.append(lista[x])
for x in range(m+1,len(lista)):
    n_words.append(lista[x])
#pdb.set_trace()
from collections import defaultdict
d = defaultdict(list)
counter = 1
for item in m_words:
    d[item].append(counter)
    counter = counter + 1
#pdb.set_trace()
for item in n_words:
    #pdb.set_trace()
    #print(d[item])
    entro = False
    for value in d[item]:
        entro = True
        #pdb.set_trace()
        print(value,end=" ")
    if entro is True:
         print('')
    if entro is False:
        print(-1)