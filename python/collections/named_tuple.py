#https://www.hackerrank.com/challenges/py-collections-namedtuple/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())

indice = 0
for x in range(0,len(lines[1].split())):
    if lines[1].split()[x] == 'MARKS':
        indice = x
        break

sumatoria = 0
contador = 0
for line in lines[2:]:
    marks = line.split()[indice]
    sumatoria = sumatoria + int(marks)
    contador = contador + 1
resultado = sumatoria/contador
print(resultado)