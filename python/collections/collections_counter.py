# https://www.hackerrank.com/challenges/collections-counter/problem
from collections import Counter
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
number_of_shoes = lines[0]
shoe_sizes = map(int,lines[1].split(' '))
contador_de_zapatos = Counter(shoe_sizes)
number_of_customers = lines[2]
suma_ganada = 0
for x in range(3,len(lines)):
    size_price = lines[x].split(' ')
    size = int(size_price[0])
    price = int(size_price[1])
    if contador_de_zapatos[size] > 0:
        contador_de_zapatos[size] = contador_de_zapatos[size] - 1
        suma_ganada = suma_ganada + price
print(suma_ganada)