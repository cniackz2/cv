# https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list/problem?h_r=next-challenge&h_v=zen
def function(n,arr):
    conjunto = set(arr)
    conjunto = list(conjunto)
    conjunto.sort()
    print(conjunto[-2])
if __name__ == '__main__':
    n = 4
    arr = map(int, '57 57 -57 57'.split())
    function(n,arr)