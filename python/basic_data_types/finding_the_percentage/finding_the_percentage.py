def funcion(student_marks,query_name):
    lista = student_marks[query_name]
    promedio = 0
    for calificacion in lista:
        promedio = promedio + calificacion
    promedio = promedio / len(lista)
    round(promedio,2)
    print("%.2f" % promedio)
if __name__ == '__main__':
    student_marks = {'Krishna': [67.0, 68.0, 69.0], 'Arjun': [70.0, 98.0, 63.0], 'Malika': [52.0, 56.0, 60.0]}
    query_name = "Malika"
    funcion(student_marks,query_name)