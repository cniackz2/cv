# https://www.hackerrank.com/challenges/python-lists/problem
import fileinput
inputs = []
for line in fileinput.input():
    inputs.append(line.rstrip())
inputs = inputs[1:]
final_list = []
for operation in inputs:
    if 'insert' in operation:
        insert_list = operation.split(' ')
        insert_list = insert_list[1:]
        final_list.insert(int(insert_list[0]),int(insert_list[1]))
    if 'print' in operation:
        print(final_list)
    if 'remove' in operation:
        index = int(operation.split(' ')[1])
        final_list.remove(index)
    if 'append' in operation:
        value = int(operation.split(' ')[1])
        final_list.append(value)
    if 'sort' in operation:
        final_list.sort()
    if 'pop' in operation:
        final_list.pop()
    if 'reverse' in operation:
        final_list.reverse()
    #import pdb; pdb.set_trace()