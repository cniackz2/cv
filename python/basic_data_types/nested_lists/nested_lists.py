#https://www.hackerrank.com/challenges/nested-list/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
def funcion(diccionario):
    values_sorted = sorted(diccionario.values())
    conjunto = set(values_sorted)
    conjunto = list(conjunto)
    conjunto.sort()
    calificacion = conjunto[1]
    tmp = []
    for key in diccionario:
        if diccionario[key] == calificacion:
            tmp.append(key)
    tmp.sort()
    for item in tmp:
        print(item)
if __name__ == '__main__':
    diccionario = {'Harry': 37.21, 'Berry': 37.21, 'Tina': 37.2, 'Akriti': 41.0, 'Harsh': 39.0}
    funcion(diccionario)