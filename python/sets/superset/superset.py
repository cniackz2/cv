#https://www.hackerrank.com/challenges/py-check-strict-superset/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(line.rstrip())
A = set(map(int,lines.pop(0).split()))
n = int(lines.pop(0))
result = False
for i in range(n):
	B = set(map(int,lines.pop(0).split()))
	result = A.issuperset(B)
	if result is False:
		break
print(result)