#https://www.hackerrank.com/challenges/py-check-subset/
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
conjuntos = []
for x in range(2,len(lines),2):
    conjuntos.append(set(map(int,lines[x].split())))
for x in range(0,len(conjuntos),2):
    #pdb.set_trace()
    print(conjuntos[x].issubset(conjuntos[x+1]))
#pdb.set_trace()
