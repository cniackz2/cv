#https://www.hackerrank.com/challenges/py-set-intersection-operation/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
#pdb.set_trace()
conjunto_a = set(map(int,lines[1].split()))
conjunto_b = set(map(int,lines[3].split()))
conjunto_c = conjunto_a.intersection(conjunto_b)
print(len(conjunto_c))
