#https://www.hackerrank.com/challenges/py-set-mutations/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())

m = int(lines.pop(0))
A = set(map(int, lines.pop(0).split(" ")))
n = int(lines.pop(0))

for i in range(n):
    cmd, args = lines.pop(0).split(" ")
    B = set(map(int, lines.pop(0).split(" ")))
    eval('A.'+cmd+'(B)')
print(sum(A))
