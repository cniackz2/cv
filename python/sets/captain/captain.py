import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(line.rstrip())
group_size  = lines[0]
room_number = lines[1]
pdb.set_trace()
room_number = list(map(int,room_number.split()))
conjunto = set(room_number)
diccionario = {}
for item in room_number:
    try:
        diccionario[item] = diccionario[item] + 1
    except:
        diccionario[item] = 1
result = None
for key in diccionario:
    if diccionario[key] == 1:
        result = key
print(result)