#https://www.hackerrank.com/challenges/py-set-union/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
#pdb.set_trace()
conjunto_a = set(map(int,lines[1].split()))
conjunto_b = set(map(int,lines[3].split()))
conjunto_c = conjunto_a.union(conjunto_b)
print(len(conjunto_c))
