#https://www.hackerrank.com/challenges/py-set-discard-remove-pop/problem
import pdb
import fileinput
counter = 0
conjunto = None
for line in fileinput.input():
    #pdb.set_trace()
    line = line.rstrip()
    if counter == 1:
        #pdb.set_trace()
        conjunto = sorted(set(line.split()))
    if counter > 2:
        if 'pop' in line:
            conjunto.pop(0)
        if 'remove' in line:
            try:
                conjunto.remove(line.split()[1])
            except:
                pass
        if 'discard' in line:
            try:
                conjunto.remove(line.split()[1])
            except:
                pass
    counter = counter + 1
suma = 0
for item in conjunto:
    suma = suma + int(item)
print(suma)