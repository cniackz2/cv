#https://www.hackerrank.com/challenges/symmetric-difference/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
set_one = lines[1].split()
set_two = lines[3].split()
set_one = list(map(int,set_one))
set_two = list(map(int,set_two))
set_one = set(set_one)
set_two = set(set_two)
diff_one = set_one.difference(set_two)
diff_two = set_two.difference(set_one)
final_diff = diff_one.union(diff_two)
final_diff = list(final_diff)
final_diff = sorted(final_diff)
for item in final_diff:
    print(item)