# https://www.hackerrank.com/challenges/re-group-groups/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
m = lines.pop(0)
matcher= re.compile(r'(\w)\1*')
m = [match.group() for match in matcher.finditer(m)]
something_was_printed = False
for item in m:
    #pdb.set_trace()
    if len(item) > 1:
        if None != re.match(r'[a-zA-Z0-9]',item[0]):
            print(item[0])
            something_was_printed = True
            break
if something_was_printed:
	pass
else:
	print("-1")