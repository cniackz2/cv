#https://www.hackerrank.com/challenges/re-start-re-end/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
p1 = lines.pop(0)
p2 = lines.pop(0)
lista = []
for x in range(0,len(p1)):
    #pdb.set_trace()
    fer = re.search(p2,p1[x:len(p1)])
    if fer:
        if fer.end() + x == len(p1):
            uui = fer.start()+x,fer.start()+x+len(p2)-1
            lista.append(uui)
            break
        else:
            uui = fer.start()+x,fer.start()+x+len(p2)-1
            lista.append(uui)
lista = list(sorted(set(lista)))
se_imprimio_algo = False
for item in lista:
    print(item)
    se_imprimio_algo = True
if se_imprimio_algo:
    pass
else:
    print('(-1, -1)')
#pdb.set_trace()