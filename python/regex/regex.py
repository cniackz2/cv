#https://www.hackerrank.com/challenges/introduction-to-regex/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
number_of_test_cases = int(lines.pop(0))
numbers_to_check = []
for i in range(number_of_test_cases):
    number = lines.pop(0)
    numbers_to_check.append(number)

for number in numbers_to_check:
    result = False
    # Que no contenga letras
    if re.search("[A-Z]",number) == None:
        # Que empiece con +, -, . o digito
        if re.match("^\+", number) or re.match("^\-", number) or re.match("^\.", number) or re.match("^\d", number) != None:
            # Que solo tenga un puntos
            lista = re.findall("[.]",number)
            if len(lista) == 1:
                if re.search("\.\d",number) != None:
                    try:
                        float(number)
                        result = True
                    except:
                        pass
    print(result)