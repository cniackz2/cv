#https://www.hackerrank.com/challenges/validate-a-roman-number/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
roman = lines.pop(0)
ma = re.match(r'^[M]{0,3}[CD]*[X]*[C]*[LV]{0,1}[X]{0,3}[V]*[I]{0,3}[X]*$|^[M]{0,3}[C]+[M][X]*[C]*[LV]{0,1}[X]{0,3}[V]*[I]{0,3}[X]*$',roman)
#                 MMM           M                 false
#                 MMM     C     M   X   C                        I      X true
#                  M             X      LIII   -> True

if ma:
    print('True')
else:
    print('False')
pdb.set_trace()