#https://www.hackerrank.com/challenges/validating-the-phone-number/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
number = int(lines.pop(0))
for x in range(0,number):
    phone = lines.pop(0)
    #pdb.set_trace()
    mq = re.match(r'^[7-9]\d\d\d\d\d\d\d\d\d$',phone)
    if mq:
        print("YES")
    else:
        print("NO")