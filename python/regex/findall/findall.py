#https://www.hackerrank.com/challenges/re-findall-re-finditer/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
m = lines.pop(0)

m2 = re.findall(r'([^aeiouAEIOU]*)([aeiouAEIOU]*)([^aeiouAEIOU]*)',m)
#pdb.set_trace()
got_printed = False

for x in range(0,len(m2)):
    if len(m2[x][1]) > 1:
        try:
            if m2[x][2] != '':
                print(m2[x][1])
                got_printed = True
        except:
            pdb.set_trace()

if got_printed:
    pass
else:
    print("-1")

#pdb.set_trace()