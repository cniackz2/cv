#https://www.hackerrank.com/challenges/hex-color-code/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
number_of_lines = int(lines.pop(0))
for x in range(0,number_of_lines):
    tmp = lines.pop(0)
    we_have_color = False
    if len(tmp.split(" ")) > 1:
        # we can have a color
        we_have_color = True
    else:
        pass # we don't have a color
    if we_have_color:
        ma = re.match(r'.*[\-a-z]+:.*',tmp)
        if ma:
            mb = re.findall(r'#[ABCDEFabcdef0-9]{3,6}',tmp)
            if mb:
                for item in mb:
                    print(item)