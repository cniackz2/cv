#https://www.hackerrank.com/challenges/re-split/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(line.rstrip())

regex_pattern = r"[,]|[.]"	# Do not delete 'r'.
print("\n".join(re.split(regex_pattern, lines.pop())))