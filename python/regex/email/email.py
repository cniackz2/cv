#https://www.hackerrank.com/challenges/validating-named-email-addresses/problem
import re
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
number_of_emails = int(lines.pop(0))
emails = []
emails2 = []
for x in range(0,number_of_emails):
    temp = lines.pop(0)
    emails2.append(temp)
    emails.append(temp.split(' ')[1][1:-1])
counter = 0
for email in emails:
    #pdb.set_trace()
    ma = re.match(r'^([a-zA-Z]+)([a-zA-Z0-9_\-.]*)[@]([a-zA-Z]*)[.]([a-zA-Z]{1,3})$',email)
    if ma:
        print(emails2[counter])
    else:
        pass # print nothing
    counter = counter + 1
#pdb.set_trace()