#https://www.hackerrank.com/challenges/polar-coordinates/problem
import pdb
import cmath
import fileinput
for line in fileinput.input():
    z = line.rstrip()
first_number = 0
secon_number = 0
if z[0] == '-':
    #import pdb; pdb.set_trace()
    z = z[1:]
    if '+' in z:
        z = z.split('+')
        secon_number = int(z[1][:-1])
    if '-' in z:
        z = z.split('-')
        secon_number = (-1)*int(z[1][:-1])
    first_number = (-1)*int(z[0])
else:
    if '+' in z:
        z = z.split('+')
        secon_number = int(z[1][:-1])
    if '-' in z:
        z = z.split('-')
        secon_number = (-1)*int(z[1][:-1])
    first_number = (+1)*int(z[0])
a = cmath.phase(complex(first_number, secon_number))
r = abs(complex(first_number, secon_number))
print(r)
print(a)