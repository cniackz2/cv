#https://www.hackerrank.com/challenges/python-mod-divmod/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(int(line.rstrip()))
a = lines.pop(0)
b = lines.pop(0)
print(a//b)
print(a%b)
print(divmod(a,b))