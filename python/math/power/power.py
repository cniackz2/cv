#https://www.hackerrank.com/challenges/python-power-mod-power/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(int(line.rstrip()))
a = lines.pop(0)
b = lines.pop(0)
m = lines.pop(0)
print(pow(a,b))
print(pow(a,b,m))