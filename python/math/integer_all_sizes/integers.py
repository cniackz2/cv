#https://www.hackerrank.com/challenges/python-integers-come-in-all-sizes/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(int(line.rstrip()))
a = int(lines[0])
b = int(lines[1])
c = int(lines[2])
d = lines[3]
print(a**b + c**d)