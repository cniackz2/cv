#https://www.hackerrank.com/challenges/map-and-lambda-expression/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())

numero = int(lines.pop(0))

def Fibonacci(n):
    if n==0:
        return 0
    elif n==1:
        return 1
    elif n==2:
        return 1
    else:
        return Fibonacci(n-1)+Fibonacci(n-2)

# Driver Program
numeros = []
for x in range(0,numero):
    numeros.append(Fibonacci(x))

final_list = []
for item in numeros:
    sum = lambda item: item * item * item
    final_list.append(sum(item))
print(final_list)