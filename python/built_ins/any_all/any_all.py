#https://www.hackerrank.com/challenges/any-or-all/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
N = int(lines.pop(0))
m = lines.pop(0).split()
nk = list(map(int,m))
for item in nk:
    if item > 0:
        result = True
    else:
        result = False
        break
#result = all(nk)
if result is True:
    for item in m:
        if item == item[::-1]:
            result = True
            break
        else:
            result = False
print(result)