#https://www.hackerrank.com/challenges/zipped/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
N_X = lines.pop(0).split()
N = int(N_X[0]) # Number of Students
X = int(N_X[1]) # X subjects (materias)
subjects = []
for i in range(X):
    subjects.append(list(map(float,lines.pop(0).split())))
result = list(zip(*subjects))
for item in result:
    promedio = 0
    suma = 0
    for item2 in item:
        suma = suma + float(item2)
    #import pdb; pdb.set_trace()
    promedio = suma / X
    print("{:.1f}".format(promedio))
