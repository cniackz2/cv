#https://www.hackerrank.com/challenges/input/problem
import pdb
import fileinput
lines = []
for line in fileinput.input():
	lines.append(line.rstrip())
x_k = lines.pop(0)
x = int(x_k.split()[0])
k = int(x_k.split()[1])
P = lines.pop(0)
if eval(P) == k:
	print(True)
else:
	print(False)
