#https://www.hackerrank.com/challenges/itertools-product/problem
import fileinput
from itertools import product
inputs = []
for line in fileinput.input():
    inputs.append(line)
list_a = map(int,inputs[0].rstrip().split(' '))
list_b = map(int,inputs[1].rstrip().split(' '))
list_c = list(product(list_a,list_b))
for item in list_c:
    print(item,end=' ')
print()