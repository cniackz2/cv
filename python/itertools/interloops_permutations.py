# https://www.hackerrank.com/challenges/itertools-permutations/problem
import fileinput
from itertools import permutations
entrada = ''
for line in fileinput.input():
    entrada = line
lista = entrada.split(' ')
a = sorted(lista[0])
k = int(lista[1])
for p in permutations(a, k):
    print("".join(p))