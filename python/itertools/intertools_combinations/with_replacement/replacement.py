#https://www.hackerrank.com/challenges/itertools-combinations-with-replacement/problem
import pdb
import fileinput
from itertools import combinations_with_replacement
lines = []
for line in fileinput.input():
    lines.append(line.rstrip())
lines = lines[0].split()
lines[0] = sorted(lines[0])
lines[0] = ''.join(lines[0])
#pdb.set_trace()
new_list = list(combinations_with_replacement(lines[0],int(lines[1])))
for item in new_list:
    for item2 in item:
        print(item2,end='')
    print('')