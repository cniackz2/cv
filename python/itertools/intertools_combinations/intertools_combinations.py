#https://www.hackerrank.com/challenges/itertools-combinations/problem
import fileinput
lines = []
for line in fileinput.input():
    lines = line.rstrip()
from itertools import combinations
#import pdb
#pdb.set_trace()
for x in range(1,int(lines.split()[1])+1):
    the_list = list(combinations(sorted(lines.split()[0]),x))
    for item in the_list:
        #import pdb; pdb.set_trace()
        print("".join(item))